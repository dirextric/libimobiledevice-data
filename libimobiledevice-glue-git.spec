%global libimobiledevice_glue_revision REPLACEME_GLUE_REVISION
%global libimobiledevice_glue_version REPLACEME_GLUE_VERSION
%global libimobiledevice_glue_tag REPLACEME_GLUE_TAG

Name:             libimobiledevice-glue-git
Summary:          Library with common code used around libimobiledevice
Version:          %{libimobiledevice_glue_version}
Release:          1%{?dist}
License:          LGPLv2+
URL:              https://www.libimobiledevice.org/
Source0:          https://github.com/libimobiledevice/libimobiledevice-glue/archive/%{libimobiledevice_glue_revision}.tar.gz

BuildRequires:    autoconf automake libtool
BuildRequires:    gcc gcc-c++
BuildRequires:    libplist-git-devel
BuildRequires:    make

%description
libimobiledevice-glue is a library with common code used by the libraries and
tools around the libimobiledevice project.

%package          devel
Summary:          Development files for libimobiledevice-glue-git
Requires:         %{name}%{?_isa} = %{version}-%{release}
Requires:         pkgconfig

%description      devel
Development files for libimobiledevice-glue-git.

%prep
%autosetup -n libimobiledevice-glue-%{libimobiledevice_glue_revision}
echo %{libimobiledevice_glue_tag} > .tarball-version
./autogen.sh

%build
%configure --prefix=%{_prefix} --libdir=%{_libdir} --disable-static

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find $RPM_BUILD_ROOT -type f -name "*.la" -delete

%files
%license COPYING
%doc README.md
%{_libdir}/libimobiledevice-glue-1.0.so.0*

%files devel
%{_libdir}/pkgconfig/libimobiledevice-glue-1.0.pc
%{_libdir}/libimobiledevice-glue-1.0.so
%{_includedir}/libimobiledevice-glue
