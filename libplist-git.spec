%global libplist_revision REPLACEME_LIBPLIST_REVISION
%global libplist_version REPLACEME_LIBPLIST_VERSION
%global libplist_tag REPLACEME_LIBPLIST_TAG

Name:             libplist-git
Summary:          Library to handle Apple Property List format in binary or XML
Version:          %{libplist_version}
Release:          1%{?dist}
License:          LGPLv2+
URL:              https://www.libimobiledevice.org/
Source0:          https://github.com/libimobiledevice/libplist/archive/%{libplist_revision}.tar.gz

BuildRequires:    autoconf automake libtool
BuildRequires:    gcc gcc-c++
BuildRequires:    make
BuildRequires:    python3-devel

%description
libplist is a small portable C library to handle Apple Property List files in
binary or XML format.

%package          devel
Summary:          Development files for libplist-git
Requires:         %{name}%{?_isa} = %{version}-%{release}
Requires:         pkgconfig

%description      devel
Development files for libplist-git.

%prep
%autosetup -n libplist-%{libplist_revision}
echo %{libplist_tag} > .tarball-version
./autogen.sh

%build
%configure --prefix=%{_prefix} --libdir=%{_libdir} --disable-static

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find $RPM_BUILD_ROOT -type f -name "*.la" -delete

%files
%license COPYING.LESSER
%doc README.md
%{_bindir}/plistutil
%{_libdir}/libplist-2.0.so.3*
%{_libdir}/libplist++-2.0.so.3*
%{_mandir}/man1/*

%files devel
%{_libdir}/pkgconfig/libplist-2.0.pc
%{_libdir}/pkgconfig/libplist++-2.0.pc
%{_libdir}/libplist-2.0.so
%{_libdir}/libplist++-2.0.so
%{_includedir}/plist
