%global libimobiledevice_revision REPLACEME_IDEVICE_REVISION
%global libimobiledevice_version REPLACEME_IDEVICE_VERSION
%global libimobiledevice_tag REPLACEME_IDEVICE_TAG

Name:             libimobiledevice-git
Summary:          Library to communicate with iOS devices
Version:          %{libimobiledevice_version}
Release:          1%{?dist}
License:          LGPLv2+
URL:              https://www.libimobiledevice.org/
Source0:          https://github.com/libimobiledevice/libimobiledevice/archive/%{libimobiledevice_revision}.tar.gz

BuildRequires:    autoconf automake libtool
BuildRequires:    gcc gcc-c++
BuildRequires:    libimobiledevice-glue-git-devel
BuildRequires:    libplist-git-devel
BuildRequires:    libusbmuxd-git-devel
BuildRequires:    openssl-devel

%description
libimobiledevice is a library to communicate with services on iOS devices using
native protocols.

%package          devel
Summary:          Development files for libimobiledevice-git
Requires:         %{name}%{?_isa} = %{version}-%{release}
Requires:         pkgconfig

%description      devel
Development files for libimobiledevice-git.

%prep
%autosetup -n libimobiledevice-%{libimobiledevice_revision}
echo %{libimobiledevice_tag} > .tarball-version
./autogen.sh

%build
%configure --prefix=%{_prefix} --libdir=%{_libdir} --disable-static

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find $RPM_BUILD_ROOT -type f -name "*.la" -delete

%files
%license COPYING.LESSER
%doc README.md
%{_bindir}/idevice*
%{_datadir}/man/man1/idevice*
%{_libdir}/libimobiledevice-1.0.so.6*

%files devel
%{_libdir}/pkgconfig/libimobiledevice-1.0.pc
%{_libdir}/libimobiledevice-1.0.so
%{_includedir}/libimobiledevice/
%{_includedir}/*.h
