%global libusbmuxd_revision REPLACEME_LIBUSBMUXD_REVISION
%global libusbmuxd_version REPLACEME_LIBUSBMUXD_VERSION
%global libusbmuxd_tag REPLACEME_LIBUSBMUXD_TAG

Name:             libusbmuxd-git
Summary:          Client library to multiplex connections from and to iOS devices
Version:          %{libusbmuxd_version}
Release:          1%{?dist}
License:          LGPLv2+
URL:              https://www.libimobiledevice.org/
Source0:          https://github.com/libimobiledevice/libusbmuxd/archive/%{libusbmuxd_revision}.tar.gz

BuildRequires:    autoconf automake libtool
BuildRequires:    gcc gcc-c++
BuildRequires:    libimobiledevice-glue-git-devel
BuildRequires:    libplist-git-devel
BuildRequires:    make

%description
libusbmuxd is a client library to multiplex connections from and to iOS devices
alongside command-line utilities.

%package          devel
Summary:          Development files for libusbmuxd-git
Requires:         %{name}%{?_isa} = %{version}-%{release}
Requires:         pkgconfig

%description      devel
Development files for libusbmuxd-git.

%prep
%autosetup -n libusbmuxd-%{libusbmuxd_revision}
echo %{libusbmuxd_tag} > .tarball-version
./autogen.sh

%build
%configure --prefix=%{_prefix} --libdir=%{_libdir} --disable-static

sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%install
%make_install

find $RPM_BUILD_ROOT -type f -name "*.la" -delete

%files
%license COPYING
%doc README.md
%{_bindir}/inetcat
%{_bindir}/iproxy
%{_libdir}/libusbmuxd-2.0.so.*
%{_mandir}/man1/*

%files devel
%{_libdir}/pkgconfig/libusbmuxd-2.0.pc
%{_libdir}/libusbmuxd-2.0.so
%{_includedir}/usbmuxd*
